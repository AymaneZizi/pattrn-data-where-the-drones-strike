.PHONY: pattrn-data clean

PATTRN_DATA_FOLDER := pattrn-data/data/pattrn-data-where-the-drones-strike

all: pattrn-data

clean:
	rm -rf ./tmp/*
	rm -rf ./pattrn-data
	rm -rf ./node_modules

pattrn-data:
	mkdir -p $(PATTRN_DATA_FOLDER)
	mkdir -p ./tmp
	cp source-metadata/config.json pattrn-data/config.json
	cp source-metadata/metadata.json $(PATTRN_DATA_FOLDER)/metadata.json
	cp source-metadata/settings.json $(PATTRN_DATA_FOLDER)/settings.json
	npm install && npm run --silent json2yaml source-metadata/metadata.json > $(PATTRN_DATA_FOLDER)/metadata.yaml
	Rscript scripts/where-the-drones-strike.R
	cp tmp/data.geojson $(PATTRN_DATA_FOLDER)/data.geojson
